package challenge

import (
	"fmt"

	"github.com/beckler/matasano/utilities"
)

func Challenge1() {
	out, _ := utilities.HexToBase64([]byte("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"))
	fmt.Println("Challenge 1: " + string(out))
}
