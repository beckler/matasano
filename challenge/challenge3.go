package challenge

import (
	"github.com/beckler/matasano/utilities"
)

func Challenge3(input []byte) []byte {
	solution, _ := SolveSingleXOR(input)
	return solution
}

func SolveSingleXOR(input []byte) ([]byte, float64) {
	input, _ = utilities.DecodeHex(input)
	highScore := float64(0)
	highKey := byte(0)
	for i := rune('A'); i < rune('z'); i++ {
		decoded := utilities.SingleByteXOR(byte(i), input)
		score := utilities.ScoreEnglish(decoded)

		if score > highScore {
			highScore = score
			highKey = byte(i)
		}
	}
	return utilities.SingleByteXOR(highKey, input), highScore
}
