package challenge

import (
	"fmt"

	"github.com/beckler/matasano/utilities"
)

func Challenge2() {
	in1, _ := utilities.DecodeHex([]byte("1c0111001f010100061a024b53535009181c"))
	in2, _ := utilities.DecodeHex([]byte("686974207468652062756c6c277320657965"))
	out, _ := utilities.FixedXOR(in1, in2)
	fmt.Println("Challenge 2: " + string(utilities.EncodeHex(out)))
}
