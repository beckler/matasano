package challenge

import (
	"bufio"
	"fmt"
	"os"
)

func Challenge4() {
	file, _ := os.Open("files/4.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	bestScore := float64(0)
	bestOutput := []byte{}
	for scanner.Scan() {
		output, score := SolveSingleXOR([]byte(scanner.Text()))
		if score > bestScore {
			bestScore = score
			bestOutput = output
		}
	}
	fmt.Println(string(bestOutput))
}
