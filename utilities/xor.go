package utilities

import (
	"errors"
)

func FixedXOR(one, two []byte) ([]byte, error) {
	if len(one) != len(two) {
		return nil, errors.New("Mismatched XOR lengths")
	}
	output := make([]byte, len(one))
	for i := 0; i < len(one); i++ {
		output[i] = one[i] ^ two[i]
	}
	return output, nil
}

func SingleByteXOR(key byte, input []byte) []byte {
	output := make([]byte, len(input))
	for i, value := range input {
		output[i] = key ^ value
	}
	return output
}
