package utilities

import (
	"encoding/base64"
	"encoding/hex"
)

func DecodeHex(input []byte) ([]byte, error) {
	output := make([]byte, hex.DecodedLen(len(input)))
	if _, err := hex.Decode(output, input); err != nil {
		return nil, err
	}
	return output, nil
}

func EncodeHex(input []byte) []byte {
	output := make([]byte, hex.EncodedLen(len(input)))
	hex.Encode(output, input)
	return output
}

func HexToBase64(input []byte) ([]byte, error) {
	if hexOutput, err := DecodeHex(input); err != nil {
		return nil, err
	} else {
		output := make([]byte, base64.StdEncoding.DecodedLen(len(input)))
		base64.StdEncoding.Encode(output, hexOutput)
		return output, nil
	}
}
