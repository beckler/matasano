package utilities

import "bytes"

type Letter struct {
	Value     byte
	Frequency float64
}

type Alphabet []Letter

func (a Alphabet) Contains(value byte) bool {
	for _, letter := range a {
		if letter.Value == value {
			return true
		}
	}
	return false
}

func (a Alphabet) Frequency(value byte) float64 {
	for _, letter := range a {
		if letter.Value == value {
			return letter.Frequency
		}
	}
	return 0
}

var English = Alphabet{
	{' ', 0.15000},
	{'E', 0.12702},
	{'T', 0.09056},
	{'A', 0.08167},
	{'O', 0.07507},
	{'I', 0.06966},
	{'N', 0.06749},
	{'S', 0.06327},
	{'H', 0.06094},
	{'R', 0.05987},
	{'D', 0.04253},
	{'L', 0.04025},
	{'C', 0.02782},
	{'U', 0.02758},
	{'M', 0.02406},
	{'W', 0.02360},
	{'F', 0.02228},
	{'G', 0.02015},
	{'Y', 0.01974},
	{'P', 0.01929},
	{'B', 0.01492},
	{'V', 0.00978},
	{'K', 0.00772},
	{'J', 0.00153},
	{'X', 0.00150},
	{'Q', 0.00095},
	{'Z', 0.00074},
}

func ScoreEnglish(input []byte) float64 {
	input = bytes.ToUpper(input)
	frequency := make(map[byte]float64)

	for _, letter := range input {
		if English.Contains(letter) {
			frequency[letter] += 1
		}
	}

	n := float64(len(frequency))
	score := float64(0)

	for letter, count := range frequency {
		optimal := English.Frequency(letter) * 100
		score -= count - (optimal * n)
	}

	return score
}
